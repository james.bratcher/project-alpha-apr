from django.shortcuts import render, redirect
from projects.views import project_list
from tasks.models import Task
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url="/accounts/login/")
def task_create(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect(project_list)
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required(login_url="/accounts/login/")
def task_list(request):
    tasking = Task.objects.filter(assignee=request.user)
    context = {
        "tasking": tasking,
    }
    return render(request, "tasks/list.html", context)
