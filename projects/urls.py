from django.urls import path
from projects.views import project_list, project_show, project_create

urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", project_show, name="show_project"),
    path("create/", project_create, name="create_project"),
]
