from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


@login_required(login_url="/accounts/login/")
def project_list(request):
    lists = Project.objects.filter(owner=request.user)
    context = {
        "lists": lists,
    }
    return render(request, "projects/list.html", context)


@login_required(login_url="/accounts/login/")
def project_show(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required(login_url="/accounts/login/")
def project_create(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            new_project = form.save(commit=False)
            new_project.owner = request.user
            new_project.save()
            return redirect(project_list)
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
